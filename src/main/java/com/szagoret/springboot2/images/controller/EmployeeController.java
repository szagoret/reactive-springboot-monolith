package com.szagoret.springboot2.images.controller;

import com.szagoret.springboot2.images.model.Employee;
import com.szagoret.springboot2.images.service.EmployeeService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;

@RestController
public class EmployeeController {

    private EmployeeService employeeService;

    public EmployeeController(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    @GetMapping("/employees")
    public Flux<Employee> findEmployees() {
        return employeeService.findEmployees();
    }
}
