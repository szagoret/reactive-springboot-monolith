package com.szagoret.springboot2.images.service;

import com.szagoret.springboot2.images.model.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.mongodb.core.ReactiveMongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;

@Service
public class EmployeeService {

    @Autowired
    ReactiveMongoOperations operations;

    public Flux<Employee> findEmployees() {
        Employee employee = new Employee();
        employee.setLastName("baggins");

        ExampleMatcher matcher = ExampleMatcher.matching()
                .withIgnoreCase()
                .withMatcher("lastName", ExampleMatcher.GenericPropertyMatchers.startsWith())
                .withIgnoreNullValues();

        Example<Employee> example = Example.of(employee, matcher);

        return operations.find(new Query(Criteria.byExample(example)), Employee.class);
    }
}
