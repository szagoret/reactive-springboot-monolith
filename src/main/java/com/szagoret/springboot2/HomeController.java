package com.szagoret.springboot2;

import com.szagoret.springboot2.comments.CommentReaderRepository;
import com.szagoret.springboot2.images.model.Image;
import com.szagoret.springboot2.images.service.ImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.codec.multipart.FilePart;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.io.IOException;
import java.util.HashMap;

@Controller
public class HomeController {

    private static final String BASE_PATH = "/images";
    private static final String FILENAME = "{filename:.+}";

    @Autowired
    private ImageService imageService;

    @Autowired
    private CommentReaderRepository commentReaderRepository;


    /**
     * The updated thymeleaf-spring5 module (version 3.0.5.M3) introduces new support for producing Sever-Sent Events (SSE)
     * when Thymeleaf is used for rendering the view of an HTTP request with content type "Accept: text/event-stream" and
     * what is executed is a template containing an iteration (th:each) on a reactive data-driver variable (a Flux<?>)
     * <p>
     * In scenarios in which the JSON received at the browser via SSE events is parsed and then used to render fragments
     * of HTML (DOM) by means of a client-side template engine, Thymeleaf could completely save this client-side processing
     * step by sending already-rendered HTML from the server instead of JSON, using exactly the same template code used
     * for normal server-side HTML rendering.
     *
     * @param model
     * @return
     */
    @GetMapping("/")
    public Mono<String> index(Model model) {
        model.addAttribute("images", imageService.findAllImages()
                .flatMap(image -> Mono.just(image)
                        .zipWith(commentReaderRepository.findByImageId(image.getId()).collectList())
                        .map(imageAndComments -> new HashMap<String, Object>() {
                            {
                                put("id", imageAndComments.getT1().getId());
                                put("name", imageAndComments.getT1().getName());
                                put("comments", imageAndComments.getT2());
                            }
                        })));
        model.addAttribute("extra", "DevTools can also detect code changes too");
        return Mono.just("index");
    }

    /**
     * In this case, subscribing is handled by the framework when a request comes in
     *
     * @param filename
     * @return
     */
    @GetMapping(value = BASE_PATH + "/" + FILENAME + "/raw", produces = MediaType.IMAGE_JPEG_VALUE)
    public Mono<ResponseEntity> oneRawImage(@PathVariable String filename) {
        return imageService.findOneImage(filename)
                .map(resource -> {
                    try {
                        return ResponseEntity.ok()
                                .contentLength(resource.contentLength())
                                .body(new InputStreamResource(
                                        resource.getInputStream()
                                ));
                    } catch (IOException e) {
                        return ResponseEntity.badRequest()
                                .body("Couldn't find " + filename
                                        + " => " + e.getMessage());
                    }
                });
    }

    @PostMapping(value = BASE_PATH)
    public Mono<String> createFile(@RequestPart(name = "file")
                                           Flux<FilePart> files) {
        return imageService.createImage(files)
                .then(Mono.just("redirect:/"));
    }

    @DeleteMapping(BASE_PATH + "/" + FILENAME)
    public Mono<String> deleteFile(@PathVariable String filename) {
        return imageService.deleteImage(filename)
                .then(Mono.just("redirect:/"));
    }


    @GetMapping(BASE_PATH)
    public Flux<Image> images() {
        return imageService.findAllImages();
    }
}
