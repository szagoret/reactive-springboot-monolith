package com.szagoret.springboot2;

import com.szagoret.springboot2.comments.Comment;
import com.szagoret.springboot2.images.model.Employee;
import com.szagoret.springboot2.images.model.Image;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.stereotype.Component;
import org.springframework.util.FileCopyUtils;
import org.springframework.util.FileSystemUtils;
import org.springframework.web.filter.reactive.HiddenHttpMethodFilter;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.UUID;

import static com.szagoret.springboot2.LocalPaths.UPLOAD_DIR;


@SpringBootApplication
public class ReactiveSpringBootApplication {

    private static org.slf4j.Logger logger = LoggerFactory.getLogger(ReactiveSpringBootApplication.class);

    public static void main(String[] args) {

        SpringApplication.run(ReactiveSpringBootApplication.class, args);
    }


    /**
     * make the HTTP DELETE methods work properly
     */
    @Bean
    HiddenHttpMethodFilter hiddenHttpMethodFilter() {
        return new HiddenHttpMethodFilter();
    }

    @Component
    public class InitDatabase {
        @Bean
        CommandLineRunner init(MongoOperations mongoOperations) {
            return args -> {
                mongoOperations.dropCollection(Employee.class);

                Employee e1 = new Employee();
                e1.setId(UUID.randomUUID().toString());
                e1.setFirstName("Bilbo");
                e1.setLastName("Baggins");
                e1.setRole("burglar");

                mongoOperations.insert(e1);

                Employee e2 = new Employee();
                e2.setId(UUID.randomUUID().toString());
                e2.setFirstName("Frodo");
                e2.setLastName("Baggins");
                e2.setRole("ring bearer");

                mongoOperations.insert(e2);


                // delete existing images folder and images from DB
                FileSystemUtils.deleteRecursively(new File(UPLOAD_DIR));
                mongoOperations.dropCollection(Image.class);

                // create a new folder from view images
                Files.createDirectory(Paths.get(UPLOAD_DIR));

                // iterate each file in the folder and copy in the new folder
                // insert each copied file into DB
                Files.newDirectoryStream(Paths.get("images")).forEach(path -> {
                    try {
                        String fileName = path.getFileName().toString();
                        logger.info("Copy file {} and insert into DB", fileName);

                        FileCopyUtils.copy(path.toFile(), Paths.get(UPLOAD_DIR, fileName).toFile());
                        mongoOperations.insert(new Image(UUID.randomUUID().toString(), fileName));

                    } catch (IOException e) {
                        logger.error(e.getMessage());
                    }
                });

                // extract all images and log them into console
                logger.info("========================= Show inserted images =========================");
                mongoOperations.findAll(Image.class).forEach(image -> logger.info(image.getName()));
                logger.info("================================ --- ===================================");
            };
        }

        @Bean
        CommandLineRunner setUp(MongoOperations operations) {
            return args -> operations.dropCollection(Comment.class);
        }
    }
}
