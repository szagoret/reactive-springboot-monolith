package com.szagoret.springboot2;

import com.szagoret.springboot2.images.model.Image;
import org.junit.Test;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

public class ImageTests {

    @Test
    public void imagesManagedByLombokShouldWork() {
        Image image = new Image("1", "file-name.jpg");

        assertThat(image.getId()).isEqualTo("1");
        assertThat(image.getName()).isEqualTo("file-name.jpg");
    }
}
